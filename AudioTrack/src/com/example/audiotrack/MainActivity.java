package com.example.audiotrack;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	private final Handler handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		findViewById(R.id.button1).setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Runnable notification = new Runnable() {
	        public void run() {
	        	TextView text = (TextView)findViewById(R.id.textMsg);
	        	
	        	
	        	
	        	int bufsize = AudioTrack.getMinBufferSize(
	        			44100,
	        			AudioFormat.CHANNEL_CONFIGURATION_STEREO,
	        			AudioFormat.ENCODING_PCM_16BIT);
	        	
	        	
	        	AudioTrack mAudioTrack = new AudioTrack(
	        			AudioManager.STREAM_MUSIC,
	        			44100, 
	        			AudioFormat.CHANNEL_CONFIGURATION_STEREO,
	        			AudioFormat.ENCODING_PCM_16BIT,
	        			bufsize,
	        			AudioTrack.MODE_STREAM);
	        	
	        	text.setText("This is onClick trigger." + bufsize);
	        	
	        	
	        	
	        	System.out.println("======1");
	        	
	        	InputStream in = null;
	        	try{
	        		in = MainActivity.this.getAssets().open("fen01.pcm");
	        	} catch (FileNotFoundException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        		System.out.println("1-1!!!!!!!!!!!!!!!!!!!!!!!");
	        		return;
	        	} catch (IOException e) {
					// TODO Auto-generated catch block
	        		System.out.println("1-2!!!!!!!!!!!!!!!!!!!!!!!");
					e.printStackTrace();
				}
	        	
	        	System.out.println("======2");
	        	
	        	mAudioTrack.play();
	        	
	        	int c = 0;
	        	byte audioBuffer[]=new byte[bufsize];
	        	
	        	try {
					while((c=in.read(audioBuffer))!=-1){
						if (mAudioTrack.getPlayState() != AudioTrack.PLAYSTATE_PAUSED) {  
                            mAudioTrack.write(audioBuffer, 0, audioBuffer.length);  
                        } else {  
                            try {  
                                Thread.sleep(1000);  
                            } catch (InterruptedException e) {  
                                // TODO Auto-generated catch block  
                                e.printStackTrace();  
                            }  
                        }
//						mAudioTrack.write(buffer, 0, buffer.length);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("2!!!!!!!!!!!!!!!!!!!!!!!");
					return ;
				}
	        	
	        	System.out.println("======3");
	        	
	        	mAudioTrack.stop();
	        	mAudioTrack.release();
			}
	    };
	    handler.post(notification);
	}

	
	
	

}
