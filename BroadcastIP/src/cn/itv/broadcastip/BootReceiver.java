package cn.itv.broadcastip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
	static final String ACTION = "android.intent.action.BOOT_COMPLETED";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(ACTION)) {
			context.startService(new Intent(context, IPService.class));// 启动倒计时服务
			// 这边可以添加开机自动启动的应用程序代码
		}
	}

}
