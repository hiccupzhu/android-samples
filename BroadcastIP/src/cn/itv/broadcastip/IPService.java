package cn.itv.broadcastip;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class IPService extends Service {

	private ServerRunnable serverRunnable = null;

	private DatagramSocket serverReceiveSocket = null;
	
	private int port = 45231;
	
	private Handler handler = new Handler();

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		serverRunnable = new ServerRunnable();
		serverRunnable.start();
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		Toast.makeText(getApplicationContext(), "ServerRunnable stop" , Toast.LENGTH_SHORT).show();
		serverRunnable = null;
		super.onDestroy();
	}

	class ServerRunnable extends Thread {
		@Override
		public void run() {
			handler.post(new Runnable() {
				
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), "ServerRunnable start" , Toast.LENGTH_SHORT).show();
				}
			});
			try {
				byte[] buf = new byte[1024];

				while (this == serverRunnable) {
					if (serverReceiveSocket == null) {
						try {
							serverReceiveSocket = new DatagramSocket(port);
							handler.post(new Runnable() {
								
								@Override
								public void run() {
									Toast.makeText(getApplicationContext(), "Port:" + port , Toast.LENGTH_SHORT).show();
								}
							});
						} catch (Exception e) {
							port++;
						}
					}

					assert (serverReceiveSocket != null);

					try {
						DatagramPacket pack = new DatagramPacket(buf, 1024);
						serverReceiveSocket.setReceiveBufferSize(1024);
						serverReceiveSocket.receive(pack);
						final String s = decodePacket(pack);
						System.out.println("xxoo--" + s);
						handler.post(new Runnable() {
							
							@Override
							public void run() {
								Toast.makeText(getApplicationContext(), new Date().toLocaleString() + "--" + s, Toast.LENGTH_SHORT).show();
							}
						});
						
					} catch (Throwable e) {
						e.printStackTrace();
						if (serverReceiveSocket != null) {
							serverReceiveSocket.close();
							serverReceiveSocket = null;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (serverReceiveSocket != null) {
					serverReceiveSocket.close();
					serverReceiveSocket = null;
				}
			}
		}
	}

	private String decodePacket(DatagramPacket pack) {
		String str = null;
		try {
			byte[] head = new byte[4];
			System.arraycopy(pack.getData(), 0, head, 0, head.length);
			int length = byteToInt(head);
			byte[] body = new byte[length];
			System.arraycopy(pack.getData(), head.length, body, 0, body.length);
			str = new String(body, "UTF-8");
		} catch (Exception e) {
			return null;
		}
		return str;
	}

	public int byteToInt(byte[] b) {
		if (b == null || b.length != 4)
			return 0;
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i] & 0x000000FF) << shift;
		}
		return value;
	}
}
