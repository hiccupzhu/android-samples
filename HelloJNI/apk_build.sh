#!/bin/bash

CWD=`pwd`
ANDROID_JAR="/usr/local/adt-bundle-linux-x86-20140321/sdk/platforms/android-17/android.jar"

echo ""
aapt package -f -m -J ./gen -S res -M AndroidManifest.xml -I $(ANDROID_JAR) 

javac -target 1.6 -bootclasspath $(ANDROID_JAR) -d bin src/com/example/hellojni/*.java gen/com/example/hellojni/R.java 

dx --dex --output=$(CWD)/bin/classes.dex $(CWD)/bin

aapt package -f -M AndroidManifest.xml -S res -I $(ANDROID_JAR) -F bin/resources.ap_

apkbuilder $(CWD)/bin/projectdemo.apk -v -u -z $(CWD)/bin/resources.ap_ -f $(CWD)/bin/classes.dex -rf $(CWD)/src  
