package com.example.hellojni;
 
import android.app.Activity;
import android.util.Log;
import android.widget.TextView;
import android.os.Bundle;
 
 
public class HelloJni extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	System.out.println("==================1");
        super.onCreate(savedInstanceState);
        System.out.println("==================2");
        TextView  tv = new TextView(this);
        tv.setText( stringFromJNI() );
        
        System.out.println("==================3");
        setContentView(tv);
    }
    public native String  stringFromJNI();
    static {
    	System.out.println("==================4");
        System.loadLibrary("hello-jni");
        System.out.println("==================5");
    }
}
