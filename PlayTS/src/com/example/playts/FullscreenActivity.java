package com.example.playts;

import java.io.IOException;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;

public class FullscreenActivity extends Activity {
	private SurfaceView surfaceView = null;
	private MediaPlayer mPlayer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_fullscreen);
		surfaceView = (SurfaceView) findViewById(R.id.player_surface);
		mPlayer = new MediaPlayer();
		surfaceView.getHolder().addCallback(new Callback() {
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mPlayer.setDisplay(null);
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				mPlayer.setDisplay(holder);
			}
		});

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				try {
					mPlayer.reset();
//					mPlayer.setDataSource(FullscreenActivity.this, Uri.parse("http://192.168.2.8/Earth_TR2_1080.m3u8"));
					mPlayer.setDataSource(FullscreenActivity.this, Uri.parse("/mnt/nfs/opt/Earth_TR2_1080.ts"));
					mPlayer.prepare();
					mPlayer.start();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}, 5000);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    System.out.println("================ onKeyDown() trigger");
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			mPlayer.stop();
			mPlayer.release();
			System.exit(0);
		}
		return super.onKeyDown(keyCode, event);
	}
}
