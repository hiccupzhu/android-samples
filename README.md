# Welcome To Android Sample
---

## [Introduce]

These are some test-projects for AndroidOS.


## [Projects]

- **appcompat_v7**

        New ADT auto-make it, and now HelloJNI depended it 

- **AudioTrack**

        Android audio-track samples, it can play some simple PCM file.
        PCM: samples:44100Hz channels:2

- **HelloJNI**

        For Android JNI test, you can use ant build it.

- **Mp3_Player**  

        Now useless ...

- **Streaming_Audio_Player**

        Simple audio player.

#[Contact]
    If you have any questions, Pls communication with me.

    Mail: hiccupzhu@gmail.com
